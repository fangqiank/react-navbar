import React from 'react'
import { Link, useResolvedPath, useMatch } from 'react-router-dom'

const CustomLink = ({ to, children, ...props }) => {
	const resolvePath = useResolvedPath(to)
	const isActive = useMatch({path:resolvePath.pathname,end:true})

	return (
		<li className={isActive ? 'active' : ''}>
			<Link to={to} {...props}>
				{children}
			</Link>
		</li>
	)
}

export const Navbar = () => {
	return (
		<nav className='nav'>
			<Link to="/" className="site-title">
        Site Name
      </Link>
			<ul>
				<CustomLink to="/pricing">Pricing</CustomLink>
        <CustomLink to="/about">About</CustomLink>
			</ul>
		</nav>
	)
}
