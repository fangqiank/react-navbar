import { Navbar } from './components/Navbar'
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import {Home} from './Pages/Home'
import {Price} from './Pages/Price'
import {About} from './Pages/About'

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <div className="container">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/pricing" element={<Price />} />
              <Route path="/about" element={<About />} />
            </Routes>
        </div>
      </Router>
    </>
  )
}

export default App
